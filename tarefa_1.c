#include <stdio.h>
#include <stdlib.h>

#define MAXN 1000

int id[MAXN], sz[MAXN];

void init(int n) {
    for (int i = 0; i < n; i++) {
        id[i] = i;
        sz[i] = 1;
    }
}

int root(int i) {
    while (i != id[i]) {
        id[i] = id[id[i]]; // path compression
        i = id[i];
    }
    return i;
}

int connected(int p, int q) {
    return root(p) == root(q);
}

void unite(int p, int q) {
    int i = root(p);
    int j = root(q);
    if (i == j) return;
    if (sz[i] < sz[j]) {
        id[i] = j;
        sz[j] += sz[i];
    } else {
        id[j] = i;
        sz[i] += sz[j];
    }
}

int main() {
    int n, p, q;
    scanf("%d", &n);
    init(n);
    while (scanf("%d %d", &p, &q) == 2 && p >= 0 && q >= 0) {
        if (connected(p, q)) continue;
        unite(p, q);
        printf("%d %d\n", p, q);
    }
    return 0;
}

