#include stdio.h
#include stdlib.h
#include string.h

#define TABLE_SIZE 100

struct node {
    char* key;
    int value;
    struct node* next;
};

struct hashtable {
    int size;
    struct node** table;
}

int hash(char* key) {
    int hashval = 0;
    for (int i = 0; i < strlen(key); i++) {
        hashval = key[i] + (hashval << 5) - hashval;
    }
    return hashval % TABLE_SIZE;
}

struct node* create_node(char* key, int value) {
    struct node* newnode = (struct node*) malloc(sizeof(struct node));
    newnode->key = strdup(key);
    newnode->value = value;
    newnode->next = NULL;
    return newnode;
}

struct hashtable* create_table(int size) {
    struct hashtable* newtable = (struct hashtable*) malloc(sizeof(struct hashtable));
    newtable->size = size;
    newtable->table = (struct node**) calloc(size, sizeof(struct node*));
    return newtable;
}

void insert(struct hashtable* table, char* key, int value) {
    int index = hash(key);
    struct node* newnode = create_node(key, value);
    if (table->table[index] == NULL) {
        table->table[index] = newnode;
    } else {
        struct node* current = table->table[index];
        while (current->next != NULL) {
            current = current->next;
        }
        current->next = newnode;
    }
}

void remove_node(struct hashtable* table, char* key) {
    int index = hash(key);
    struct node* current = table->table[index];
    struct node* previous = NULL;
    while (current != NULL) {
        if (strcmp(current->key, key) == 0) {
            if (previous == NULL) {
                table->table[index] = current->next;
            } else {
                previous->next = current->next;
            }
            free(current->key);
            free(current);
            return;
        }
        previous = current;
        current = current->next;
    }
}

int search(struct hashtable* table, char* key) {
    int index = hash(key);
    struct node* current = table->table[index];
    while (current != NULL) {
    	      if (strcmp(current->key, key)
  
		
